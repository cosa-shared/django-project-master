django-project
==============

This small project is to implement Django framework with a couple of test cases and an extra third-party app: django-sample-app.

    python manage.py syncdb --noinput
    coverage run manage.py test --settings mysite.test_settings  # to test polls app and produce coverage data
    coverage html -include=polls/* --omit=polls/tests/*          # visit html output at htmlcov/index.html
    python manage.py runserver # (user: admin, passwd: admin)
